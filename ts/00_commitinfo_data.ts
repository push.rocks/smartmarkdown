/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartmarkdown',
  version: '3.0.1',
  description: 'do more with markdown files'
}
