import * as plugins from './smartmarkdown.plugins.js';

export class MdParsedResult {
  public static async createFromMarkdownString(mdStringArg: string): Promise<MdParsedResult> {
    const mdParsedResult = new MdParsedResult();
    await mdParsedResult.updateFromMarkdownString(mdStringArg);
    return mdParsedResult;
  }

  public originalString: string;
  public title: string;
  public html: string;
  public frontmatterData: {[key: string]: any};

  public async updateFromMarkdownString(mdStringArg: string) {
    let yamlString: string;
    const result = await plugins.unified()
      .use(plugins.remarkParse)
      .use(plugins.remarkGfm)
      .use(plugins.remarkFrontmatter, ['yaml', 'toml'])
      .use(plugins.remarkStringify)
      .use(plugins.remarkHtml)
      .use(() => (tree) => {
        console.dir(tree);
        const yamlChild = tree.children.find(objectArg => objectArg.type === 'yaml');
        if (yamlChild) {
          yamlString = (yamlChild as any).value;
        }
      })
      .process(mdStringArg);
    this.html = result.toString();
    if (yamlString) {
      this.frontmatterData = await plugins.smartyaml.yamlStringToObject(yamlString);
    } else {
      this.frontmatterData = {};
    }
  }
}
