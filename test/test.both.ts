import { expect, tap } from '@pushrocks/tapbundle';
import * as smartmarkdown from '../ts/index.js';

let smartMarkdownInstance: smartmarkdown.SmartMarkdown;

tap.test('should create a valid instance of SmartMarkdown', async () => {
  smartMarkdownInstance = new smartmarkdown.SmartMarkdown();
  expect(smartMarkdownInstance).toBeInstanceOf(smartmarkdown.SmartMarkdown);
});

tap.test('should convert a markdown string to html', async () => {
  const markdownString = '# Hi!';
  const mdParsedResult = await smartMarkdownInstance.getMdParsedResultFromMarkdown(markdownString);
  const htmlString = mdParsedResult.html;
  expect(htmlString).toEqual('<h1>Hi!</h1>\n');
});

tap.test('should get frontmatter', async () => {
  const markdownString = `---
hello: yes
---

# hello there
`;
  const mdParsedResult = await smartMarkdownInstance.getMdParsedResultFromMarkdown(markdownString);
  
  expect(mdParsedResult.frontmatterData.hello).toEqual('yes');
  
});

tap.test('should convert a html string to markdown', async () => {
  const htmlString = '<h1 id="hi">Hi!</h1>\n<h2>This is it!</h2>';
  const markdownString = smartMarkdownInstance.htmlToMarkdown(htmlString);
  console.log(markdownString);
});

tap.start();
